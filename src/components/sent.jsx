import React, { Component } from 'react';
import { Table } from 'antd';


const columns = [
    {
        title: 'Username',
        dataIndex: 'username'
    },
    {
        title: 'City',
        dataIndex: 'city',
    },
    {
        title: 'website',
        dataIndex: 'website',
    },
];

const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    }
};


export default class Sent extends Component {


    state = {
        loading: true,
        columns: null,
        data: null
    }

    async componentDidMount() {
        const url = "https://jsonplaceholder.typicode.com/users";
        const response = await fetch(url);
        var data = await response.json();
        var users = [];

        data.map(function (item) {
            users.push({
                "key": item.id,
                "username": item.username,
                "city": item.address.city,
                "website": item.website
            })
        })

        this.setState({ loading: false, data: users });
        console.log(users);
    }

    render() {
        return (
            <div>
                {

                    this.state.loading ?
                        <div style={{ textAlign: 'center' }}>Loading.. </div>
                        :
                        <Table
                            rowSelection={{
                                type: 'checkbox',
                                ...rowSelection,
                            }}
                            columns={columns}
                            dataSource={this.state.data}
                            pagination={{ defaultPageSize: 6}}
                        />
                }

            </div>
        );
    }
}