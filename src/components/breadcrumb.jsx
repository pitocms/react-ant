import React,{Component} from 'react';
import { Breadcrumb, Row, Col, Empty } from 'antd';
import {
	useParams,
	Switch,
	Route,
	Link,
	
} from "react-router-dom";


export default class BreadCrumb extends Component{

	// constructor(props) {
	// 	super(props);
	// 	this.state = {
	// 		page: "Inbox"
	// 	};
	// }

	// changePage = ()=>({
	// 	this.abc  = "inbox"
	// });


	render(){
		// console.log(this.state.page);
		return(
			
			<Row style={{ marginTop: '80px', marginBottom: '10px', marginRight: "10px" }}>
				<Col span={11} style={{textTransform:"uppercase",marginTop:"-10px",marginLeft:"10px"}}>
					<Switch>
						{/* {this.state.page} */}
						<Route path="/:id" children={<Child />} />
					</Switch>
				</Col>
				<Col span={12} style={{ textAlign: "right"}}>
				
					<Breadcrumb separator=">">
						<Breadcrumb.Item >
						    <Link to='/'>
								Home
							</Link>
						</Breadcrumb.Item>
						<Breadcrumb.Item>
							<Link to='/inbox'>
								<Route path="/:id" children={<Child />} />
							</Link>
						</Breadcrumb.Item>
					</Breadcrumb>
					
			    </Col>
			</Row>
		)
	}
} 

function Child() {
	let { id } = useParams();

	return (
		<span>
			{id}
		</span>
	);
}