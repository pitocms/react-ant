import React,{Component} from 'react';
import { Menu, Switch } from 'antd';
import { Link } from 'react-router-dom';
import {
  MailOutlined,
  CalendarOutlined,
  AppstoreOutlined,
  SettingOutlined,
  CopyOutlined,
  FunnelPlotOutlined,
  SendOutlined,
  BorderOutlined,
} from '@ant-design/icons';

const { SubMenu } = Menu;

export default class LeftSidebar extends Component{

    state = {
	    mode: 'inline',
	    theme: 'light',
	};

	changeMode = value => {
	    this.setState({
	      mode: value ? 'vertical' : 'inline',
	    });
	};

	changeTheme = value => {
	    this.setState({
	      theme: value ? 'dark' : 'light',
	    });
	};

	render(){
	return (
      <div>
        
        <Menu
          style={{ minHeight: "100%", hight:"100%" }}
          defaultSelectedKeys={['1']}
          defaultOpenKeys={['sub1']}
          mode={this.state.mode}
          theme={this.state.theme}
        >
          <Menu.Item key="1">
            <Link to="/inbox">
                 <MailOutlined />
                 Inbox
            </Link>
          </Menu.Item>

          <Menu.Item key="2">
            <Link to="/sent">
              <SendOutlined />
              Sent
            </Link>
          </Menu.Item>

          <Menu.Item key="3">
            <Link to="/drafts">
              <CopyOutlined />
              Drafts
            </Link>
          </Menu.Item>

          <Menu.Item key="4">
            <Link to="/junk">
              <FunnelPlotOutlined />
              Junk
            </Link>
          </Menu.Item>

          <SubMenu
            key="sub1"
            title={
              <span>
                <AppstoreOutlined />
                <span>More</span>
              </span>
            }
          >

            <Menu.Item key="5">
              <Link to="/spam">
                Spam
              </Link>
            </Menu.Item>

            <Menu.Item key="6">
              <Link to="/trash">
                 Trash
              </Link>
            </Menu.Item>

          </SubMenu>
          

          <Menu.Item key="7" style={{color:'#dd4b39'}}>
            <Link to="/important">
              <BorderOutlined />
              Important
            </Link>
          </Menu.Item>

          <Menu.Item key="8" style={{color:'#f39c12'}} >
            <Link to="/promotions">
              <BorderOutlined />
              Promotions
            </Link>
          </Menu.Item>

          <Menu.Item key="9" style={{color:'#3c8dbc'}}>
            <Link to="/social">
              <BorderOutlined />
              Socia
            </Link>
          </Menu.Item>

        </Menu>
      </div>
    );
	}
} 