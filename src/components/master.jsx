import React,{Component} from 'react';
import { Layout,Menu,Breadcrumb,Row,Col  } from 'antd';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import TopNav from './top-nav';
import BreadCrumb from './breadcrumb';
import LeftSidebar from './leftsidebar';
import Inbox from './inbox';
import Sent from './sent';


const Test = ({match}) =>{
	return (<h1>Hello Test {match.params.username}</h1>);
}

export default class Master extends Component{
    render(){
    	return(
		    <div>
				<Router>
		    	< TopNav/ >
		    	<Row>
		    	  <Col xs={{span: 24}} md={{span:5}} style={{ marginTop:'60px'}}>
		    		< LeftSidebar/>
		    	  </Col>
                  
		    	  <Col span={19}  >
		    	    < BreadCrumb/ >

					<Switch>
						<Route path="/" exact component={Inbox} />
						<Route path="/inbox" exact component={Inbox} />
						<Route path="/sent" exact component={Sent} />
						<Route path="/drafts" exact component={Sent} />
						<Route path="/junk" exact component={Sent} />
						<Route path="/trash" exact component={Sent} />
						<Route path="/compose" exact component={Sent} />
						<Route path="/spam" exact component={Sent} />
						<Route path="/important" exact component={Sent} />
						<Route path="/promotions" exact component={Sent} />
						<Route path="/social" exact component={Sent} />
						<Route path="/compose" exact component={Sent} />
					</Switch>
		    	  	
		    	  </Col>
		    	</Row>
				</Router>
		    </div>
    	)
    }
}