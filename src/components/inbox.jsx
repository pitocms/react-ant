import React,{Component } from 'react';
import { Table } from 'antd';


const columns = [
  {
    title: 'Name',
    dataIndex: 'name'
  },
  {
    title: 'Email',
    dataIndex: 'email',
  },
  {
    title: 'Phone',
    dataIndex: 'phone',
  },
];

const rowSelection = {
	  onChange: (selectedRowKeys, selectedRows) => {
	    console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
	  }
};


export default class Inbox extends Component{
  

  state = {
      loading : true,
      columns : null,
      data    : null
  }

  async componentDidMount(){
    const url = "https://jsonplaceholder.typicode.com/users";
    const response = await fetch(url);
    var data = await response.json();
    var users = [];

    data.map(function (item) {
      users.push({
        "key"  : item.id,
        "name" : item.name,
        "email": item.email,
        "phone": item.phone
      })
    })

    this.setState({loading:false,data:users});
    console.log(users);
  }

	render(){
		return (
		    <div>
          {
          
              this.state.loading ? 
              <div style={{ textAlign: 'center' }}>Loading.. </div>
              :
              <Table
                rowSelection={{
                  type: 'checkbox',
                  ...rowSelection,
                }}
                columns={columns}
                dataSource={this.state.data}
                pagination={{ defaultPageSize: 6 }}
              />
          }
		      
		    </div>
		  );
	}
}