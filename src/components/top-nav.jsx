import React,{Component} from 'react';
import { Layout,Menu,Breadcrumb  } from 'antd';

const { Header } = Layout;


export default class TopNav extends Component{
    render(){
    	return(
		    <Layout>
			    <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
			      <div className="logo" />
			      <Menu
			        theme="dark"
			        mode="horizontal"
			        defaultSelectedKeys={['1']}
			        style={{ lineHeight: '64px' }}
			      >
			        <Menu.Item key="1">Home</Menu.Item>
			        <Menu.Item key="2" style={{ float : "right" }}>LogIn</Menu.Item>
			      </Menu>
			    </Header>
		    </Layout>
    	)
    }
}